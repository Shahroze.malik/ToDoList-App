FROM ubuntu:18.04
WORKDIR /app
ADD . /app
RUN apt-get update -y && apt-get install -y python3-pip python3-dev
RUN pip3 install -r requirements.txt
ENV FLASK_APP=ToDoList.py
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
CMD ["flask", "run", "-h", "0.0.0.0"]

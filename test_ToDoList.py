from ToDoList import app
import json

def test_homepage():
    response = app.test_client().get('/', follow_redirects=True)
    assert response.status_code == 200

def test_GetItem():
    response = app.test_client().get('/GetItem', follow_redirects=True)
    assert response.status_code == 200

def test_UpdateOrDelete():
    response = app.test_client().get('/UpdateOrDelete', follow_redirects=True)
    assert response.status_code == 200
